//
//  Identifible.swift
//  Baatch
//
//  Created by jibeex on 10/15/16.
//  Copyright © 2016 Jianbin LIN. All rights reserved.
//

import UIKit

protocol Identifible{
    var id:Int {get}
}
