//
//  Customer.swift
//  Baatch
//
//  Created by jibeex on 10/12/16.
//  Copyright © 2016 Jianbin LIN. All rights reserved.
//

import UIKit

struct Customer:Locatable, Identifible{
    let id:Int
    let x:Double
    let y:Double
    var orders:[Order] = []
    
    init(id:Int, x:Double, y:Double){
        self.id = id
        self.x = x
        self.y = y
    }
}
