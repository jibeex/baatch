//
//  Rider.swift
//  Baatch
//
//  Created by jibeex on 10/12/16.
//  Copyright © 2016 Jianbin LIN. All rights reserved.
//

import UIKit

struct Rider:Locatable, Identifible{
    let id:Int
    let speed:Double
    var x:Double
    var y:Double
    
    
    /**
     * Return the time(in minutes) needed the pass the given distance
     */
    func ridingTimeFor(distance:Double) -> Double{
        return distance/speed * 60
    }
}
