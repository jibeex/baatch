//
//  Route.swift
//  Baatch
//
//  Created by jibeex on 10/12/16.
//  Copyright © 2016 Jianbin LIN. All rights reserved.
//

import UIKit

struct Segment{
    
    let to:Locatable & Identifible
    var orders:[Order] = []
    
    // to location id
    var id:Int{
        return to.id
    }
}
