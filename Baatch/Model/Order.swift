//
//  Order.swift
//  Baatch
//
//  Created by jibeex on 10/15/16.
//  Copyright © 2016 Jianbin LIN. All rights reserved.
//

import UIKit

struct Order{
    let customer:Customer
    let restaurant:Restaurant
}
