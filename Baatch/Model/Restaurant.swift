//
//  Restaurant.swift
//  Baatch
//
//  Created by jibeex on 10/12/16.
//  Copyright © 2016 Jianbin LIN. All rights reserved.
//

import UIKit

struct Restaurant:Locatable, Identifible{
    let id:Int
    let cookingTime:Double
    let x:Double
    let y:Double
}
