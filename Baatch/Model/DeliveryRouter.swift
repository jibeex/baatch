//
//  DeliveryRouter.swift
//  Baatch
//
//  Created by jibeex on 10/12/16.
//  Copyright © 2016 Jianbin LIN. All rights reserved.
//

import UIKit

/*
 * Customer waiting time --> Delivery time of the customer's latest order
 * Rider delivery time   --> Time that a rider needs to go through his route
 *
 * First goal is to minimize the average wating time of all the customers
 * Second goal is to minimize the longest delivery time of all the riders
 */

struct DeliveryRouter{
    
    private var restaurants:[Int:Restaurant] = [:]
    private var customers:[Int:Customer] = [:]
    private var riders:[Int:Rider] = [:]
    
    // Routes for each rider
    private var routes:[Int:[Segment]] = [:]
    
    // Delivery time of each rider
    private var riderDeliveryTime:[Int:Double] = [:]
    
    private var maxDeliveryTime:Double{
        return riderDeliveryTime.values.max() ?? 0
    }
    
    init(restaurants:[Restaurant], customers:[Customer], riders:[Rider]){
        
        restaurants.forEach { (r) in
            self.restaurants[r.id] = r
        }
        
        customers.forEach { (c) in
            self.customers[c.id] = c
        }
        
        riders.forEach { (r) in
            self.riders[r.id] = r
        }
    }
    
    mutating func addOrder(customerId:Int, restaurantId:Int){
        
        guard let customer = customers[customerId],
            let restaurant = restaurants[restaurantId] else{
            return
        }
        
        let order = Order(customer: customer, restaurant: restaurant)
        self.addOrder(order: order)
    }
    
    /*
     * Return the routes for a rider
     * @param for -> rider id
     */
    func route(for riderId:Int) -> [Segment]{
        return self.routes[riderId] ?? []
    }

    /*
     * Return delivery time of a customer's latest order in all routes
     * @param for -> customer id
     */
    func deliveryTime(for customer:Int) -> Int{
        
        let deliveryTime:Double = self.delivaryTimeOfCustomers(routes: self.routes) [customer] ?? 0
        return Int(ceil(deliveryTime))
    }
    
    mutating func clearAllOrders(){
        self.routes = [:]
        self.riderDeliveryTime = [:]
    }
    
    /*
     * Remove orders for a customer
     * @param for -> customer id
     */
    mutating func clearOrders(for customerId:Int){
        
        let originalRoutes = self.routes
        
        for (riderId, route) in originalRoutes{
            
            var newRoute = [Segment]()
            
            for var segment in route{
                segment.orders = segment.orders.filter({ (order) -> Bool in
                    return order.customer.id != customerId
                })
                
                if segment.orders.count > 0{
                    newRoute.append(segment)
                }
            }
            
            if newRoute.count < route.count{
                self.routes[riderId] = newRoute
                self.riderDeliveryTime[riderId] = self.totalRiderDeliveryTime(route: newRoute, rider: self.riders[riderId]!)
            }
        }
        
        self.customers[customerId]?.orders = []
    }
    
    /*
     * Calculate the total time that a rider need for go through
     * the given route
     */
    private func totalRiderDeliveryTime(route:[Segment], rider:Rider) -> Double{
        
        var total:Double = 0
        var currentLocation:Locatable = rider
        
        route.forEach { (segment) in
            let distance = currentLocation.distanceTo(locatable: segment.to)
            let ridingTime = rider.ridingTimeFor(distance: distance)
            
            if let restaurant = segment.to as? Restaurant{
                if restaurant.cookingTime >= total + ridingTime{
                    total = restaurant.cookingTime
                }
                else{
                    total += ridingTime
                }
            }
            else{
                total += ridingTime
            }
            currentLocation = segment.to
        }
        
        return total
    }
    
    /*
     * Calculate the delivery time of all customers in the given route
     */
    private func delivaryTimeOfCustomers(route:[Segment], rider:Rider) -> [Int:Double]{
        
        var customerDeliveryTime:[Int:Double] = [:]
        var totalRiderTime:Double = 0
        var currentLocation:Locatable = rider
        
        route.forEach { (route) in
            let distance = currentLocation.distanceTo(locatable: route.to)
            let ridingTime = rider.ridingTimeFor(distance: distance)
            
            if let restaurant = route.to as? Restaurant{
                if restaurant.cookingTime >= totalRiderTime + ridingTime{
                    totalRiderTime = restaurant.cookingTime
                }
                else{
                    totalRiderTime += ridingTime
                }
            }
            else{
                totalRiderTime += ridingTime
            }
            
            currentLocation = route.to
            
            if let customer = currentLocation as? Customer{
                customerDeliveryTime[customer.id] = totalRiderTime
            }
        }
        
        return customerDeliveryTime
    }
    
    /*
     * Calculate the delivery time of all customers in the given list of routes
     *
     * Given n as the order number
     * Time complexity: n
     */
    private func delivaryTimeOfCustomers(routes:[Int:[Segment]]) -> [Int:Double]{
        
        var customerDeliveryTime:[Int:Double] = [:]
        
        routes.forEach { (riderId, route) in
            if let rider = self.riders[riderId]{
                delivaryTimeOfCustomers(route: route, rider: rider).forEach({ (customerId, time) in
                    if let oldMaxTime = customerDeliveryTime[customerId]{
                        if oldMaxTime < time{
                            customerDeliveryTime[customerId] = time
                        }
                    }
                    else{
                        customerDeliveryTime[customerId] = time
                    }
                })
            }
        }
        return customerDeliveryTime
        
    }
    
    
    
    /*
     * Calculate the average delivery time of all customers in the given list of routes
     *
     * Given n as the order number
     * Time complexity: n
     */
    private func averageDelivaryTimeOfCustomers(routes:[Int:[Segment]]) -> Double{
        return Array(delivaryTimeOfCustomers(routes: routes).values).average
    }
    
    
    /**
     * Create a new route by adding a new order into the original routes
     * with minimum additional average waiting time & rider delivery time
     *
     * Algorithm: test all possible ways to add the given order into routes of the given rider (Brute force)
     * Given n as the order number
     * Time complexity: n * customer number * restaurant number
     */
    private func createBestRoute(orderToAdd:Order, riderId:Int) -> (route:[Segment], riderDeliveryTime:Double, customerAverageWaitingTime:Double){
        
        var minimumCustomerWatingTime:Double = Double.infinity
        var minimumRiderDeliveryTime:Double = Double.infinity
        var bestRoute:[Segment] = []
        
        let route = self.routes[riderId] ?? []
        let rider = self.riders[riderId]!
        for restaurantIdx in 0...(route.count){
            // Contstain: customerIdx > restaurantIdx
            // Because the customer should always be reached after rider has fetched his/her meal from a restaurant
            for customerIdx in (restaurantIdx + 1)...(route.count + 1){
                var newRoute = route
                newRoute.insert(Segment(to:orderToAdd.restaurant, orders: [orderToAdd]), at: restaurantIdx)
                newRoute.insert(Segment(to:orderToAdd.customer, orders: [orderToAdd]), at: customerIdx)
                var newRoutes = self.routes
                newRoutes[riderId] = newRoute
                
                let customerAverageWatingTime = self.averageDelivaryTimeOfCustomers(routes: newRoutes)
                let riderDeliveryTime = self.totalRiderDeliveryTime(route: newRoute, rider: rider)
                
                if customerAverageWatingTime < minimumCustomerWatingTime || (minimumCustomerWatingTime == customerAverageWatingTime && minimumRiderDeliveryTime > riderDeliveryTime){
                    minimumCustomerWatingTime = customerAverageWatingTime
                    minimumRiderDeliveryTime = riderDeliveryTime
                    bestRoute = newRoute
                }
            }
        }
        
        // Remove duplicated sequent segments
        var optimizedRoute:[Segment] = []
        
        for (index, segment) in bestRoute.enumerated(){
            if index > 0{
                // If the segment has the same destination as the previous segment
                if segment.to.id == optimizedRoute[optimizedRoute.count - 1].to.id{
                    // Instead of append this segment to the new route, add all of its orders to the previous segment
                    optimizedRoute[optimizedRoute.count - 1].orders += segment.orders
                    continue
                }
            }
            optimizedRoute.append(segment)
        }
        
        return  (route:optimizedRoute, riderDeliveryTime:minimumRiderDeliveryTime, customerAverageWaitingTime:minimumCustomerWatingTime)
    }
    
    /**
     * Add an order into the route of one of the riders and minimize the additional delivery time
     *
     * Algorithm: generate optimized route for each of the rider by using createBestRoute() against the new order, select the one that has mininum additional average waiting time & rider delivery time
     * Given n as the order number
     * Time complexity: n * customer number * restaurant number * rider number
     */
    private mutating func addOrder(order:Order){
        
        var minCustomerWatingTime:Double = Double.infinity
        var minRiderDeliveryTime:Double = Double.infinity
        var bestRoute:[Segment] = []
        var bestRiderId = 0
        
        for (riderId, _) in self.riders{
            let (newRoute, newRiderDeliveryTime, newCustomerWaitingTime) = self.createBestRoute(orderToAdd: order, riderId: riderId)
            
            if newCustomerWaitingTime < minCustomerWatingTime || (newCustomerWaitingTime == minCustomerWatingTime && newRiderDeliveryTime < minRiderDeliveryTime){
                
                minRiderDeliveryTime = newRiderDeliveryTime
                minCustomerWatingTime = newCustomerWaitingTime
                bestRoute = newRoute
                bestRiderId = riderId
            }
        }
        
        self.customers[order.customer.id]?.orders.append(order)
        self.routes[bestRiderId] = bestRoute
    }

}
