//
//  Place.swift
//  Baatch
//
//  Created by jibeex on 10/15/16.
//  Copyright © 2016 Jianbin LIN. All rights reserved.
//

import UIKit

protocol Locatable{
    var x:Double {get}
    var y:Double {get}
    
    func distanceTo(locatable:Locatable) -> Double
}

extension Locatable{
    func distanceTo(locatable:Locatable) -> Double{
        
        let xDiff = self.x - locatable.x
        let yDiff = self.y - locatable.y
        
        return sqrt(xDiff * xDiff + yDiff * yDiff)
    }
}
