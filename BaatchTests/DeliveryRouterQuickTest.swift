/*
 
 This file is a simple Swift Quick (https://github.com/Quick/Quick) test suite
 describing a DeliveryRouter class.
 
 Your goals are to:
 1) make those tests pass,
 2) add tests for the edge cases.
 
 Feel free to ask us any question!
 
 Happy hacking :)
 
 --
 
 Goal: delight your customers by sending riders to pick and deliver their orders
 in less than 60 minutes (faster if you can).
 
 Hypothesis:
 - (x, y) coordinates are used to locate restaurants, customers and riders on a grid
 - the distance between x=0 and x=1 is 1000 meters (1 km)
 - the distance between two arbitrary (x,y) locations is the euclidian distance (straight line)
 - times are expressed in minutes
 - speeds are expressed in kilometers per hour (km/h)
 
 Note: some other hypothesis you can make to simplify the problem:
 - all customer orders are received at the same time (t=0)
 - all restaurants start cooking at the same time (t=0)
 - all riders start moving at the same time (t=0)
 - each restaurant can cook an infinite number of meals in parallel
 - each rider can carry an infinite amount of meals at the same time
 - riders are ninjas! They fly over trafic jams and buildings, they never need to take
   a break and they know how to solve a NP-complete problem in polynomial time ;)
 
 */

import XCTest
import Quick
import Nimble
@testable import Baatch

class DeliveryRouterSpec: QuickSpec {

    // MARK: Properties

    override func spec() {
        describe("#route") {
            let restaurants = [Restaurant(id: 3, cookingTime: 15, x: 0, y: 0),
                               Restaurant(id: 4, cookingTime: 35, x: 5, y: 5),
                               Restaurant(id: 7, cookingTime: 3, x: 4, y: 4),
                               Restaurant(id: 8, cookingTime: 3, x: 5, y: 5)]
            let customers = [Customer(id: 1, x: 1, y: 1),
                             Customer(id: 2, x: 5, y: 1),
                             Customer(id: 5, x: 1, y: 1),
                             Customer(id: 6, x: 6, y: 6)]
            let riders = [Rider(id: 1, speed: 10, x: 2, y: 0),
                          Rider(id: 2, speed: 10, x: 1, y: 0)]
            var deliveryRouter = DeliveryRouter(restaurants: restaurants, customers: customers, riders: riders)

            context("given customer 1 orders from restaurant 3") {
                beforeEach {
                    deliveryRouter.clearAllOrders()
                    deliveryRouter.addOrder(customerId: 1, restaurantId: 3)
                }

                it("sends rider 2 to customer 1 through restaurant 3") {
                    let route = deliveryRouter.route(for: 2)
                    expect(route.count).to(equal(2))
                    expect(route[0].id).to(equal(3))
                    expect(route[1].id).to(equal(1))
                }

                it("delights customer 1") {
                    expect(deliveryRouter.deliveryTime(for: 1)) < 60
                    expect(deliveryRouter.deliveryTime(for: 1)) == 15 + Int(ceil(sqrt(2)/10.0 * 60))
                }

                context("given customer 2 does not order anything") {
                    beforeEach {
                        deliveryRouter.clearOrders(for: 2)
                    }

                    it("does not assign a route to rider 1") {
                        let route = deliveryRouter.route(for: 1)
                        expect(route).to(beEmpty())
                    }

                }
                
                context("given customer 1 & 2 don't order anything") {
                    beforeEach {
                        deliveryRouter.clearOrders(for: 1)
                        deliveryRouter.clearOrders(for: 2)
                    }
                    
                    it("does not assign a route to rider 1") {
                        let route = deliveryRouter.route(for: 1)
                        expect(route).to(beEmpty())
                    }
                    
                    it("does not assign a route to rider 2") {
                        let route = deliveryRouter.route(for: 2)
                        expect(route).to(beEmpty())
                    }
                }
            }

            context("given that customers send 2 orders to 2 different restaurants") {
                beforeEach {
                    deliveryRouter.clearAllOrders()
                    deliveryRouter.addOrder(customerId: 1, restaurantId: 3)
                    deliveryRouter.addOrder(customerId: 2, restaurantId: 4)
                }

                it("sends rider 1 to customer 2 through restaurant 4") {
                    let route = deliveryRouter.route(for: 1)
                    expect(route.count).to(equal(2))
                    expect(route[0].id).to(equal(4))
                    expect(route[1].id).to(equal(2))
                }

                it("sends rider 2 to customer 1 through restaurant 3") {
                    let route = deliveryRouter.route(for: 2)
                    expect(route.count).to(equal(2))
                    expect(route[0].id).to(equal(3))
                    expect(route[1].id).to(equal(1))
                }

                it("delights customer 1") {
                    expect(deliveryRouter.deliveryTime(for: 1)) < 60
                }
                
                it("delights customer 2") {
                    expect(deliveryRouter.deliveryTime(for: 2)) < 60
                }
            }
            
            context("given that one customer sends 2 orders to 2 far-away restaurants") {
                beforeEach {
                    deliveryRouter.clearAllOrders()
                    deliveryRouter.addOrder(customerId: 2, restaurantId: 3)
                    deliveryRouter.addOrder(customerId: 2, restaurantId: 4)
                }
                
                it("sends rider 1 to customer 2 through restaurant 4") {
                    let route = deliveryRouter.route(for: 1)
                    expect(route.count).to(equal(2))
                    expect(route[0].id).to(equal(4))
                    expect(route[1].id).to(equal(2))
                }
                
                it("sends rider 2 to customer 2 through restaurant 3") {
                    let route = deliveryRouter.route(for: 2)
                    expect(route.count).to(equal(2))
                    expect(route[0].id).to(equal(3))
                    expect(route[1].id).to(equal(2))
                }
            }
            
            context("given that one customer sends 2 orders to close & fast-cooking restaurants") {
                beforeEach {
                    deliveryRouter.clearAllOrders()
                    deliveryRouter.addOrder(customerId: 6, restaurantId: 8)
                    deliveryRouter.addOrder(customerId: 6, restaurantId: 7)
                }
                
                it("sends rider 1 to customer 6 through restaurant 8 and restaurant 7") {
                    let route = deliveryRouter.route(for: 1)
                    expect(route.count).to(equal(3))
                    expect(route[0].id).to(equal(7))
                    expect(route[1].id).to(equal(8))
                    expect(route[2].id).to(equal(6))
                }
                
                it("does not assign a route to rider 2") {
                    let route = deliveryRouter.route(for: 2)
                    expect(route).to(beEmpty())
                }
            }
            
            context("given that customer 2 sends 2 orders to the same restaurants") {
                beforeEach {
                    deliveryRouter.clearAllOrders()
                    deliveryRouter.addOrder(customerId: 2, restaurantId: 4)
                    deliveryRouter.addOrder(customerId: 2, restaurantId: 4)
                }
                
                it("sends rider 1 to customer 2 through restaurant 4, only one delivery should be done") {
                    let route = deliveryRouter.route(for: 1)
                    expect(route.count).to(equal(2))
                    expect(route[0].id).to(equal(4))
                    expect(route[1].id).to(equal(2))
                }
                
                it("does not assign a route to rider 2") {
                    let route = deliveryRouter.route(for: 2)
                    expect(route).to(beEmpty())
                }
            }
            
            context("given that two next-door neiborhoods send 2 orders to the same restaurants") {
                beforeEach {
                    deliveryRouter.clearAllOrders()
                    deliveryRouter.addOrder(customerId: 1, restaurantId: 4)
                    deliveryRouter.addOrder(customerId: 5, restaurantId: 4)
                }
                
                it("sends rider 1 to customer 1 and customer 5 through restaurant 4, only one delivery should be done") {
                    let route = deliveryRouter.route(for: 1)
                    expect(route.count).to(equal(3))
                    expect(route[0].id).to(equal(4))
                    expect(route[1].id).to(equal(1) || equal(5))
                    expect(route[2].id).to(equal(1) || equal(5))
                    expect(route[1].id != route[2].id ).to(equal(true))
                }
                
                it("does not assign a route to rider 2") {
                    let route = deliveryRouter.route(for: 2)
                    expect(route).to(beEmpty())
                }
            }
        }
    }
}

